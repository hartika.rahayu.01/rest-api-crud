package main

import (
	"context"
	"encoding/json"
	"log"
	"net/http"

	"github.com/bradtraversy/REST-API-CRUD/helper"
	"github.com/bradtraversy/REST-API-CRUD/models"
	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

//Connection mongoDB with helper class
var collection = helper.ConnectDB()

func main() {
	//Init Router
	r := mux.NewRouter()

	// arrange our route
	r.HandleFunc("/api/books", getBooks).Methods("GET")
	r.HandleFunc("/api/books/{id}", getBook).Methods("GET")
	r.HandleFunc("/api/books", createBook).Methods("POST")
	r.HandleFunc("/api/books/{id}", updateBook).Methods("PUT")
	r.HandleFunc("/api/books/{id}", deleteBook).Methods("DELETE")

	//crud bookstor
	r.HandleFunc("/api/bstore", getBookstore).Methods("GET")
	r.HandleFunc("/api/bstore/{id}", getBookstores).Methods("GET")
	r.HandleFunc("/api/bstore", createBookstore).Methods("POST")
	r.HandleFunc("/api/bstore/{id}", updateBookstore).Methods("PUT")
	r.HandleFunc("/api/bstore/{id}", deleteBookstore).Methods("DELETE")

	// set our port address
	log.Fatal(http.ListenAndServe(":8000", r))

}

func getBooks(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	var books []models.Book
	cur, err := collection.Find(context.TODO(), bson.M{})

	if err != nil {
		helper.GetError(err, w)
		return
	}

	defer cur.Close(context.TODO())

	for cur.Next(context.TODO()) {

		var book models.Book
		// & character returns the memory address of the following variable.
		err := cur.Decode(&book) // decode similar to deserialize process.
		if err != nil {
			log.Fatal(err)
		}

		// add item our array
		books = append(books, book)
	}

	if err := cur.Err(); err != nil {
		log.Fatal(err)
	}

	json.NewEncoder(w).Encode(books) // encode similar to serialize process.
}

func getBookstore(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	var bookstr []models.BookStoreName
	cur, err := collection.Find(context.TODO(), bson.M{})

	if err != nil {
		helper.GetError(err, w)
		return
	}

	defer cur.Close(context.TODO())

	for cur.Next(context.TODO()) {

		var bstrs models.BookStoreName
		// & character returns the memory address of the following variable.
		err := cur.Decode(&bstrs) // decode similar to deserialize process.
		if err != nil {
			log.Fatal(err)
		}

		// add item our array
		bookstr = append(bookstr, bstrs)
	}

	if err := cur.Err(); err != nil {
		log.Fatal(err)
	}

	json.NewEncoder(w).Encode(bookstr) // encode similar to serialize process.
}

func getBook(w http.ResponseWriter, r *http.Request) {
	// set header.
	w.Header().Set("Content-Type", "application/json")

	var book models.Book
	// we get params with mux.
	var params = mux.Vars(r)

	// string to primitive.ObjectID
	id, _ := primitive.ObjectIDFromHex(params["id"])

	// We create filter. If it is unnecessary to sort data for you, you can use bson.M{}
	filter := bson.M{"_id": id}
	err := collection.FindOne(context.TODO(), filter).Decode(&book)

	if err != nil {
		helper.GetError(err, w)
		return
	}

	json.NewEncoder(w).Encode(book)
}

//get book by id
func getBookstores(w http.ResponseWriter, r *http.Request) {
	// set header.
	w.Header().Set("Content-Type", "application/json")

	var bkstore models.BookStoreName
	// we get params with mux.
	var params = mux.Vars(r)

	// string to primitive.ObjectID
	Idstore, _ := primitive.ObjectIDFromHex(params["id"])
	// book.ID = strconv.Itoa(param["id"])

	// We create filter. If it is unnecessary to sort data for you, you can use bson.M{}
	filter := bson.M{"_id": Idstore}
	err := collection.FindOne(context.TODO(), filter).Decode(&bkstore)

	if err != nil {
		helper.GetError(err, w)
		return
	}

	json.NewEncoder(w).Encode(bkstore)
}

func createBook(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	var book models.Book

	// we decode our body request params
	_ = json.NewDecoder(r.Body).Decode(&book)

	// insert our book model.
	result, err := collection.InsertOne(context.TODO(), book)

	if err != nil {
		helper.GetError(err, w)
		return
	}

	json.NewEncoder(w).Encode(result)
}

func createBookstore(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	var bkstore models.BookStoreName
	// we decode our body request params
	_ = json.NewDecoder(r.Body).Decode(&bkstore)

	// insert our book model.
	result, err := collection.InsertOne(context.TODO(), bkstore)

	if err != nil {
		helper.GetError(err, w)
		return
	}

	json.NewEncoder(w).Encode(result)
}

func updateBook(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	var params = mux.Vars(r)

	//Get id from parameters
	id, _ := primitive.ObjectIDFromHex(params["id"])

	var book models.Book

	// Create filter
	filter := bson.M{"_id": id}

	// Read update model from body request
	_ = json.NewDecoder(r.Body).Decode(&book)

	// prepare update model.
	update := bson.D{
		{"$set", bson.D{
			{"isbn", book.Isbn},
			{"title", book.Title},
			{"author", bson.D{
				{"firstname", book.Author.FirstName},
				{"lastname", book.Author.LastName},
			}},
		}},
	}

	err := collection.FindOneAndUpdate(context.TODO(), filter, update).Decode(&book)

	if err != nil {
		helper.GetError(err, w)
		return
	}

	book.ID = id

	json.NewEncoder(w).Encode(book)
}

func updateBookstore(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	var params = mux.Vars(r)

	//Get id from parameters
	Idstore, _ := primitive.ObjectIDFromHex(params["ids"])

	var bkstore models.BookStoreName

	// Create filter
	filter := bson.M{"_ids": Idstore}

	// Read update model from body request
	_ = json.NewDecoder(r.Body).Decode(&bkstore)

	// prepare update model.
	update := bson.D{
		{"$set", bson.D{
			{"StoreName", bkstore.StoreName},
		}},
	}

	err := collection.FindOneAndUpdate(context.TODO(), filter, update).Decode(&bkstore)

	if err != nil {
		helper.GetError(err, w)
		return
	}

	// bkstore.Idstore = 61e791379a40bb85fdaf88b8

	json.NewEncoder(w).Encode(bkstore)
}

func deleteBook(w http.ResponseWriter, r *http.Request) {
	// Set header
	w.Header().Set("Content-Type", "application/json")

	// get params
	var params = mux.Vars(r)

	// string to primitve.ObjectID
	id, err := primitive.ObjectIDFromHex(params["id"])

	// prepare filter.
	filter := bson.M{"_id": id}

	deleteResult, err := collection.DeleteOne(context.TODO(), filter)

	if err != nil {
		helper.GetError(err, w)
		return
	}

	json.NewEncoder(w).Encode(deleteResult)
}

func deleteBookstore(w http.ResponseWriter, r *http.Request) {
	// Set header
	w.Header().Set("Content-Type", "application/json")

	// get params
	var params = mux.Vars(r)

	// string to primitve.ObjectID
	Idstore, err := primitive.ObjectIDFromHex(params["ids"])

	// prepare filter.
	filter := bson.M{"_ids": Idstore}

	deleteResult, err := collection.DeleteOne(context.TODO(), filter)

	if err != nil {
		helper.GetError(err, w)
		return
	}

	json.NewEncoder(w).Encode(deleteResult)
}
